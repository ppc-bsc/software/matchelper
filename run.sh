docker run \
    --name matchelper \
    -p 3838:3838 \
    --restart always \
    -d \
    -v "$(pwd)"/app:/srv/shiny-server/:rw \
    -v "$(pwd)"/data:/srv/data/ \
    matchelper
