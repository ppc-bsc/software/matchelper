---
title: "Figures Paper"
output: html_document
date: "2024-12-03"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
SAVE_DIR = "../img/"
```

```{r}
library(knitr)
library(pdftools)
library(magick)
source("../app/server_functions.R")
```

```{r}
data.list <- load.data.list()
maxValues <- print(data.list$range[1,])
minValues <- print(data.list$range[2,])
```

# Radar charts
## Techniques
### Traditional model training
```{r}
data.nt <- as.data.frame(data.list$normal_training)
pdf(paste0(SAVE_DIR, "radar1_traditional_model_training.pdf"))
radarchart.single(data.nt, minValues, maxValues, title="Traditional model training")
dev.off()

image <- image_read_pdf(paste0(SAVE_DIR, "radar1_traditional_model_training.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar1_traditional_model_training.pdf"), format = "pdf")
```

### Training
```{r, include=FALSE}
data.train <- as.data.frame(data.list$data_train)

pdf(paste0(SAVE_DIR, "radar2_hypernetworks.pdf"))
radarchart.single(data.train[1,], minValues, maxValues)
dev.off()
pdf(paste0(SAVE_DIR, "radar3_quantization.pdf"))
radarchart.single(data.train[2,], minValues, maxValues)
dev.off()
pdf(paste0(SAVE_DIR, "radar4_kd.pdf"))
radarchart.single(data.train[3:4,], minValues, maxValues)
dev.off()

image <- image_read_pdf(paste0(SAVE_DIR, "radar2_hypernetworks.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar2_hypernetworks.pdf"), format = "pdf")

image <- image_read_pdf(paste0(SAVE_DIR, "radar3_quantization.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar3_quantization.pdf"), format = "pdf")

image <- image_read_pdf(paste0(SAVE_DIR, "radar4_kd.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar4_kd.pdf"), format = "pdf")
```


### Fine-tuning
```{r, include=FALSE}
data.ft <- as.data.frame(data.list$data_ft)

pdf(paste0(SAVE_DIR, "radar5_lora.pdf"))
radarchart.single(data.ft[2,], minValues, maxValues)
dev.off()
pdf(paste0(SAVE_DIR, "radar6_prunning_static.pdf"))
radarchart.single(data.ft[3,], minValues, maxValues)
dev.off()

image <- image_read_pdf(paste0(SAVE_DIR, "radar5_lora.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar5_lora.pdf"), format = "pdf")

image <- image_read_pdf(paste0(SAVE_DIR, "radar6_prunning_static.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar6_prunning_static.pdf"), format = "pdf")
```


### Test
```{r, include=FALSE}
data.infer <- as.data.frame(data.list$data_infer)

pdf(paste0(SAVE_DIR, "radar7_quantization_infer.pdf"))
radarchart.single(data.infer[1:2,], minValues, maxValues)
dev.off()
pdf(paste0(SAVE_DIR, "radar8_prunning_infer.pdf"))
radarchart.single(data.infer[3:4,], minValues, maxValues)
dev.off()

# Trimm images
image <- image_read_pdf(paste0(SAVE_DIR, "radar7_quantization_infer.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar7_quantization_infer.pdf"), format = "pdf")

image <- image_read_pdf(paste0(SAVE_DIR, "radar8_prunning_infer.pdf"))
trimmed_image <- image_trim(image)
image_write(trimmed_image, path = paste0(SAVE_DIR, "radar8_prunning_infer.pdf"), format = "pdf")
```

### Mosaic all techniques
```{r}
# Paths to the PDF files
pdf.files <- list.files(
    path = SAVE_DIR,    # Path to the folder
    pattern = "^radar",    # Regular expression to match filenames starting with "file_"
    full.names = TRUE      # Return full file paths
)

# Convert each PDF into images
image_list <- lapply(pdf.files, function(pdf) {
  pdf_convert(pdf, format = "png", dpi = 300)
})

# Flatten the list and load images using magick
images <- magick::image_read(unlist(image_list))

# Arrange images into a grid
mosaic <- magick::image_montage(
  images,
  tile = "4x2",       # Adjust grid layout (e.g., "2x2" for 4 PDFs)
  geometry = "x800"   # Set size of individual tiles
)

# Save the mosaic as a PDF
magick::image_write(mosaic, paste0(SAVE_DIR, "mosaic_techiques.png"), format = "png")

```


## Use cases
```{r}
data.usecases <- load.usecases()
for(i in 1:nrow(data.usecases)){
  usecase.i <- data.usecases[i, ]
  usecase.i.data <- usecase.i[,1:6]
  usecase.i.color <- usecase.i[,7]
  usecase.i.data <- rbind(rep(5,6), rep(1,6), usecase.i.data)
  pdf(paste0(SAVE_DIR, "uc_", rownames(usecase.i),".pdf"))
  par(xpd = TRUE, mfrow = c(1, 1))
  radarchart(mutate_all(usecase.i.data, function(x) as.numeric(x)),
    # custom polygon
    pcol = paste0(usecase.i.color, "E6"),
    pfcol = paste0(usecase.i.color, "66"),
    # custom the grid
    cglcol = "grey",
    axislabcol = "grey",
    cglty = 1,
    # custom labels
    vlcex = 1.3,
  )
  #title(rownames(usecase.i), cex.main = 1.6)
  dev.off()
  image <- image_read_pdf(paste0(SAVE_DIR, "uc_", rownames(usecase.i),".pdf"))
  trimmed_image <- image_trim(image)
  image_write(trimmed_image, path = paste0(SAVE_DIR, "uc_", rownames(usecase.i),".pdf"), format = "pdf")
}
```

### Mosaic use cases
```{r}
# Paths to the PDF files
pdf.files <- list.files(
    path = SAVE_DIR,    # Path to the folder
    pattern = "^uc_",    # Regular expression to match filenames starting with "file_"
    full.names = TRUE      # Return full file paths
)

# Convert each PDF into images
image_list <- lapply(pdf.files, function(pdf) {
  pdf_convert(pdf, format = "png", dpi = 300)
})

# Flatten the list and load images using magick
images <- magick::image_read(unlist(image_list))

# Arrange images into a grid
mosaic <- magick::image_montage(
  images,
  tile = paste0(nrow(data.usecases), "x1"),       # Adjust grid layout (e.g., "2x2" for 4 PDFs)
  geometry = "x800"   # Set size of individual tiles
)

# Save the mosaic as a PDF
magick::image_write(mosaic, paste0(SAVE_DIR, "mosaic_ucases.png"), format = "png")
```

```{r}
# Delete generated tmp PNG files
to_be_deleted <- list.files(".", pattern = "*.png")
file.remove(to_be_deleted)
```

# Clustering
```{r}
df.tecs <- load.techniques()
df.usecases <- load.usecases()
cluster.cols <- rev(c('#d7191c','#fdae61','#ffffbf','#abdda4','#2b83ba'))

y.tecs <- as.data.frame(df.tecs)
rownames(y.tecs) <- df.tecs$Technique
df.usecases <- subset(df.usecases, select=-c(color))
for(i in 1:nrow(df.usecases)){
  distmat <- distance.matrix(y.tecs, df.usecases[i,])
  pdf(paste0("../img/", rownames(df.usecases)[i] , ".pdf"), width = 12, height=8)
  clusters <- plot.distance.matrix(distmat$tecs, distmat$dist, 3, cluster.cols)
  #title(paste(rownames(df.usecases)[i]), cex.main = 2 )
  text(x=.06, y=0.05,substitute(paste(bold('Cluster'))), srt=90,cex=1.5, xpd=TRUE)
  dev.off()
  image <- image_read_pdf(paste0("../img/", rownames(df.usecases)[i] , ".pdf"))
  trimmed_image <- image_trim(image)
  image_write(trimmed_image, path = paste0("../img/", rownames(df.usecases)[i] , ".pdf"), format = "pdf")
}
```


