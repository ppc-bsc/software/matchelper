FROM rocker/shiny:4.3.3

RUN apt-get update && apt-get install -y \
    --no-install-recommends \
    git-core \
    libssl-dev \
    libcurl4-gnutls-dev \
    curl \
    libsodium-dev \
    libxml2-dev \
    libicu-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV _R_SHLIB_STRIP_=true

RUN install2.r --error --skipinstalled \
    shiny \
    dplyr \
    RColorBrewer \
    readODS \
    fmsb \
    testit \
    purrr \
    gplots \
    stringr

RUN rm -rf /srv/shiny-server/*

USER shiny

WORKDIR /srv/shiny-server/

EXPOSE 3838

CMD ["/usr/bin/shiny-server"]
